package Ejercicio4.app;

public class Ejercicio4app {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//DECLARACIÓN DE VARIABLES
		int n = 1;
		
		//VALOR INICIAL DE LA N
		System.out.println("VALOR INICIAL DE LA N: " + n);
		
		//INCREMENTO DE LA N EN 77
		n = n + 77;
		System.out.println("INCREMENTO DE N EN 77: " + n);
		
		//DECREMENTO DE LA N EN 3
		n = n -3;
		System.out.println("DECREMENTO DE LA N EN 3: " + n);
		
		//VALOR DUPLICADO DE N
		n = n * 2;
		System.out.println("VALOR DUPLICADO DE LA N: " + n);
	}

}
